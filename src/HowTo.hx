import com.haxepunk.Entity;
import com.haxepunk.Scene;
import com.haxepunk.HXP;
import com.haxepunk.graphics.Image;
import com.haxepunk.utils.Key;
import com.haxepunk.utils.Joystick;
import com.haxepunk.utils.Input;
import menu.MenuText;

class HowTo extends Scene
{
	private var focus:Int;
	private var rightArrow:MenuText;
	private var leftArrow:MenuText;

	public function new()
	{
		focus = 0;

		super();
	}

	public override function begin()
	{
		//_text:String, _x:Float, _y:Float, ?center:Bool, ?_size:Int, ?_font:String
		var txt = new MenuText("How to Play", 75, 20, true, 25, "font/TwoBits.otf");
		add(txt);

		txt = new MenuText("Space is start on keyboards.", 75, 40, true, 18, "font/TwoBits.otf");
		add(txt);

		txt = new MenuText("Green player controls:", 75, 60, true, 18, "font/TwoBits.otf");
		add(txt);

		txt = new MenuText("W/S to change selection.", 75, 70, true, 18, "font/TwoBits.otf");
		add(txt);

		txt = new MenuText("G to select/confirm.", 75, 80, true, 18, "font/TwoBits.otf");
		add(txt);

		txt = new MenuText("Orange player controls:", 75, 100, true, 18, "font/TwoBits.otf");
		add(txt);

		txt = new MenuText("Up/Down to change selection.", 75, 110, true, 18, "font/TwoBits.otf");
		add(txt);

		txt = new MenuText("Right ctrl to select/confirm.", 75, 120, true, 18, "font/TwoBits.otf");
		add(txt);

		txt = new MenuText("Gamepad:", 75, 140, true, 18, "font/TwoBits.otf");
		add(txt);

		txt = new MenuText("Up/Down to change selection.", 75, 150, true, 18, "font/TwoBits.otf");
		add(txt);

		txt = new MenuText("A to select/confirm.", 75, 160, true, 18, "font/TwoBits.otf");
		add(txt);

		txt = new MenuText("Instructions:", 75, 180, true, 18, "font/TwoBits.otf");
		add(txt);

		txt = new MenuText("There are five timelines.", 75, 190, true, 18, "font/TwoBits.otf");
		add(txt);

		txt = new MenuText("To play, move units across", 75, 200, true, 18, "font/TwoBits.otf");
		add(txt);

		txt = new MenuText("different timelines.", 75, 210, true, 18, "font/TwoBits.otf");
		add(txt);

		txt = new MenuText("You can only move units if", 75, 220, true, 18, "font/TwoBits.otf");
		add(txt);

		txt = new MenuText("you move all units in ", 75, 230, true, 18, "font/TwoBits.otf");
		add(txt);

		txt = new MenuText("a timeline at once.", 75, 240, true, 18, "font/TwoBits.otf");
		add(txt);

		txt = new MenuText("Hit Escape to exit.", 75, 260, true, 18, "font/TwoBits.otf");
		add(txt);

		/*if (HXP.fullscreen) {
			HXP.camera.x = Types._xPos;
			HXP.camera.y = Types._yPos;
			brdr = new Border(0, 0);
			add(brdr);
		}*/
	}

	public override function update() {

		/*if (!HXP.fullscreen) {
			if (HXP.camera.x < focus * 360) {
				HXP.camera.x += 10;
			} else if (HXP.camera.x > focus * 360) {
				HXP.camera.x -= 10;
			}
		} else {
			brdr.x = HXP.camera.x - 720;
			brdr.layer = -3;
			if (HXP.camera.x < focus * 360 + Types._xPos) {
				HXP.camera.x += 10;
			} else if (HXP.camera.x > focus * 360 + Types._xPos) {
				HXP.camera.x -= 10;
			}
		}*/

		if (Input.pressed(Key.ESCAPE)) {
			HXP.scene = new TitleScreen(4);
		}

		super.update();
	}
}