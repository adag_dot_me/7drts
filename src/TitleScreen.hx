import com.haxepunk.Entity;
import com.haxepunk.Scene;
import com.haxepunk.HXP;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.Sfx;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import game.part.TitleShotEmitter;
import menu.TitleUnit;
import menu.MenuText;
import flash.geom.Point;

class TitleScreen extends Scene
{
	private var state:Int;
	private var logostate:Int;
	private var menustate:Int;
	private var logo:Image;
	private var adaglogo:Spritemap;
	private var adaglogo2:Spritemap;
	private var logotimer:Array<Int>;
	private var startupsnd:Sfx;
	private var shotEmit:TitleShotEmitter;
	private var shotsnd:Sfx;
	private var unit:Array<TitleUnit>;
	private var startText:MenuText;

	public function new(?_state:Int)
	{
		super();

		if (_state > 0) {
			state = _state;
		} else {
			state = 0;
		}

		logostate = 0;
		menustate = 0;

		logotimer = new Array();
		logotimer[0] = 60;
		logotimer[1] = 10;
		logotimer[2] = 10;
		logotimer[3] = 10;
		logotimer[4] = 10;

		shotsnd = new Sfx("snd/shot.wav");
		startupsnd = new Sfx("snd/adag_on.wav");
	}

	override public function begin()
	{
		adaglogo = new Spritemap("gfx/menu/adaglogo.png", 88, 38);
		adaglogo.setFrame(0, 0);
		adaglogo.x = 31;
		adaglogo.y = 121;
		addGraphic(adaglogo);

		if (state > 0) {
			adaglogo.alpha = 0;
		}

		adaglogo2 = new Spritemap("gfx/menu/adaglogo.png", 88, 38);
		adaglogo2.setFrame(0, 2);
		adaglogo2.alpha = 0;
		adaglogo2.x = 31;
		adaglogo2.y = 121;
		addGraphic(adaglogo2);

		logo = new Image("gfx/menu/logo.png");
		logo.alpha = 0;
		logo.x = 21;
		logo.y = 40;
		addGraphic(logo);

		if (state > 1) {
			logo.alpha = 1;
		}

		//x:Float, y:Float, _frame:Int, _flip:Bool, _shotPt:Point
		unit = new Array();
		unit[0] = new TitleUnit(13, 108, 0, false, new Point(61, 140));
		unit[1] = new TitleUnit(126, 108, 1, true, new Point(89, 140));
		for (i in 0...unit.length) {
			add(unit[i]);
		}

		if (state > 3) {
			for (i in 0...unit.length) {
				unit[i].sprite.alpha = 1;
			}
		}

		//menutext constructor - _text:String, _x:Float, _y:Float, ?center:Bool, ?_size:Int, ?_font:String
		startText = new MenuText("Press Start", 75, 220, true, 20, "font/TwoBits.otf");
		startText.menuText.alpha = 0;
		add(startText);

		if (state > 4) {
			startText.menuText.alpha = 1;
		}

		shotEmit = new TitleShotEmitter();
		add(shotEmit);

		//61, 140 & 89, 140 / 32, 140 & 107, 140
	}

	override public function update() 
	{
		if (state == 0) {
			if (logostate < 5) {
				if (logotimer[logostate] <= 0) {

					if ((logostate / 2) == Std.int(logostate / 2)) adaglogo.setFrame(0, 1);
					else adaglogo.setFrame(0, 0);

					if (logostate == 4) startupsnd.play();

					logostate += 1;
				} else {
					logotimer[logostate] -= 1;
				}
			} else if (logostate == 5) {
				if (adaglogo2.alpha < 1) {
					adaglogo2.alpha += 0.01;
				} else {
					adaglogo.alpha = 0;
					logostate += 1;
				}
			} else if (logostate == 6) {
				if (adaglogo2.alpha > 0) {
					adaglogo2.alpha -= 0.01;
				} else {
					state += 1;
				}
			}
		}
		else if (state == 1) {
			if (logo.alpha < 1) {
				logo.alpha += 0.05;
			} else {
				logostate = 0;
				state += 1;
			}
		}
		else if (state == 2) {
			if (menustate < 5) {
				if ((menustate / 2) == Std.int(menustate / 2)) {
					if (unit[logostate].sprite.alpha < 1) {
						unit[logostate].sprite.alpha += 0.01;
					} else {
						shotEmit.shot(unit[logostate].shotPt.x, unit[logostate].shotPt.y, logostate);
						shotsnd.play();
						menustate += 1;
					}
				} else {
					if (unit[logostate].sprite.alpha > 0) {
						unit[logostate].sprite.alpha -= 0.01;
					} else {
						menustate += 1;
						if (logostate == 0) {
							logostate += 1;
						} else state += 1;
					}
				}
			}
		}
		else if (state == 3) {
			for (i in 0...unit.length) {
				if (unit[i].sprite.alpha < 1) {
					unit[i].sprite.alpha += 0.05;
				}
			}

			if (unit[1].sprite.alpha == 1) {
				state += 1;
			}
		}
		else if (state == 4) {
			if (startText.menuText.alpha < 1) {
				startText.menuText.alpha += 0.05;
			}

			if (Input.pressed(Key.SPACE)) {
				menustate = 0;
				state += 1;
			}
		}
		else if (state == 5) {
			if (menustate == 0) {
				startText.menuText.text = "Play game >";
				startText.menuText.centerOrigin();
			} else if (menustate == 1) {
				startText.menuText.text = "< How to Play";
				startText.menuText.centerOrigin();
			}

			if (Input.pressed(Key.LEFT)) {
				if (menustate > 0) menustate -= 1;
			}

			if (Input.pressed(Key.RIGHT)) {
				if (menustate < 1) menustate += 1;
			}

			if (Input.pressed(Key.SPACE)) {
				if (menustate == 0) {
					HXP.scene = new Level("testlevel");
				} else if (menustate == 1) {
					HXP.scene = new HowTo();
				}
			}
		}

		if (Input.pressed(Key.SPACE)) {
			if (state < 4) {
				adaglogo.alpha = 0;
				adaglogo2.alpha = 0;
				logo.alpha = 1;

				for (i in 0...unit.length) {
					unit[i].sprite.alpha = 1;
				}

				state = 4;
			}
		}

		super.update();
	}
}