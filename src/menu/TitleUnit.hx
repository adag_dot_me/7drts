package menu;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import flash.geom.Point;

class TitleUnit extends Entity
{
	public var sprite:Spritemap;
	private var spawn:Int = 0;
	public var shotPt:Point;

	public function new(x:Float, y:Float, _frame:Int, _flip:Bool, _shotPt:Point)
	{
		super(x, y);

		sprite = new Spritemap("gfx/units/basicunit.png", 12, 16);
		sprite.flipped = _flip;
		sprite.scale = 4;
		sprite.setFrame(0, _frame);
		sprite.alpha = 0;
		shotPt = _shotPt;
		graphic = sprite;
		layer = 1;
	}
	
	override public function update()
	{
		super.update();
	}
}