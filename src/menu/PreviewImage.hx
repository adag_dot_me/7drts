package menu;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;

class PreviewImage extends Entity
{
	private var sprite:Image;
	private var spawn:Int = 0;

	public function new(x:Float, y:Float, _map:String)
	{
		super(x, y);

		sprite = new Image("gfx/preview/" + _map + ".png");
		graphic = sprite;
		layer = 1;
	}
	
	override public function update()
	{
		//super.update();
	}

	public function updateSprite(_map:String) {
		sprite = new Image("gfx/preview/" + _map + ".png");
		this.graphic = sprite;
	}
}