package game.part;

import flash.display.BitmapData;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Emitter;
import com.haxepunk.utils.Ease;

class CloudEmitter extends Entity 
{
	private var _emitter:Emitter;

	public function new()
	{
		super(x, y);
		_emitter = new Emitter("gfx/cloud.png", 3, 3);
		_emitter.newType("cloud", [0]);
		_emitter.setMotion("cloud", 0, 2, 0.2, 360, 4, 0.5, Ease.quadOut);
		_emitter.setAlpha("cloud", 1, 0.1);
		graphic = _emitter;
		layer = 12;
	}

	public function cloud(_x:Float, _y:Float)
	{
		for(i in 0...20) {
			_emitter.emit("cloud", _x, _y);
		}
	}
}