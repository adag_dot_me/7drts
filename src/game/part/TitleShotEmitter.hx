package game.part;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Emitter;
import com.haxepunk.utils.Ease;

class TitleShotEmitter extends Entity 
{
	private var _emitter:Emitter;

	public function new()
	{
		super(x, y);
		_emitter = new Emitter("gfx/menu/titleshot.png", 3, 3);
		_emitter.newType("0", [0]);
		_emitter.setMotion("0", 0, 30, 0.2, 30, -4, 1, Ease.quadOut);
		_emitter.setAlpha("0", 1, 0.1);

		_emitter.newType("1", [1]);
		_emitter.setMotion("1", 180, 30, 0.2, -30, -4, 1, Ease.quadOut);
		_emitter.setAlpha("1", 1, 0.1);

		graphic = _emitter;
		layer = 0;
	}

	public function shot(_x:Float, _y:Float, _frame:Int)
	{
		trace(_x + ", " + _y + ", " + _frame);
		for(i in 0...20) {
			_emitter.emit(Std.string(_frame), _x, _y);
		}
	}
}