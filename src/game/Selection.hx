package game;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;

class Selection extends Entity
{
	public var sprite:Spritemap;
	private var alphaTimer:Int;
	private var typeNum:Int;

	public function new(x:Float, y:Float, _type:Int)
	{
		super(x, y);

		alphaTimer = 20;

		sprite = new Spritemap("gfx/selection.png", 150, 56);
		sprite.setFrame(0, _type);
		type = "select" + Std.string(_type);
		setHitbox(150, 56);
		typeNum = _type;
		graphic = sprite;
		layer = 9;
	}

	override public function update()
	{
		if (alphaTimer <= 0) {
			sprite.alpha = 1 - sprite.alpha;
			alphaTimer = 20;
		} else {
			alphaTimer -= 1;
		}

		if (collide("select" + Std.string( 1 - typeNum ), x, y) != null || collide("player" + Std.string( 1 - typeNum ), x, y) != null) {
			sprite.setFrame(1, typeNum);
		} else {
			sprite.setFrame(0, typeNum);
		}

		super.update();
	}
}