package game;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;

class BackgroundTile extends Entity
{
	public var sprite:Spritemap;
	private var spawn:Int = 0;

	public function new(x:Float, y:Float, _map:String)
	{
		super(x, y);

		sprite = new Spritemap("gfx/tiles/" + _map + ".png", 150, 56);
		graphic = sprite;
		layer = 20;
	}
}