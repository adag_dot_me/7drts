package game;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;

class Base extends Entity
{
	public var sprite:Spritemap;
	public var health:Int = 5;
	private var typeNum:Int;

	public function new(x:Float, y:Float, _type:Int)
	{
		super(x, y);

		typeNum = _type;

		sprite = new Spritemap("gfx/base.png", 16, 22);
		sprite.setFrame(0, _type);
		type = "base" + Std.string(_type);
		graphic = sprite;
		layer = 9;
	}

	override public function update() {
		var arr:Array<Entity> = new Array();
		var arr2:Array<Entity> = new Array();

		scene.getType("shot0", arr);
		scene.getType("shot1", arr2);

		for (i in 0...arr.length) {
			if (arr[i].x > x && arr[i].x < x + 16 && arr[i].y > y && arr[i].y < y + 22) {
				health -= 1;
				scene.remove(arr[i]);
			}
		}

		for (i in 0...arr2.length) {
			if (arr2[i].x > x && arr2[i].x < x + 16 && arr2[i].y > y && arr2[i].y < y + 22) {
				health -= 1;
				scene.remove(arr2[i]);
			}
		}

		if (health <= 0) {
			sprite.setFrame(1, typeNum);
		}
	}
}