package game;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.Sfx;
import flash.geom.Point;
import game.part.CloudEmitter;

class Cloud extends Entity
{
	private var cloudShape:Int;
	private var emitter:CloudEmitter;
	private var moveTimer = 20;

	public function new(x:Float, y:Float, _emitter:CloudEmitter)
	{
		super(x, y);

		emitter = _emitter;
		//cloudShape = Std.random(3);
		cloudShape = 0;
	}
	override public function update()
	{
		if (cloudShape == 0) {
			emitter.cloud(x, y);
			emitter.cloud(x - 4, y);
			emitter.cloud(x - 2, y - 4);
			emitter.cloud(x + 4, y);
		}

		x -= 0.1;

		if (x <= -10) {
			scene.remove(this);
		}

		super.update();
	}
}