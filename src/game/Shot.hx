package game;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;

class Shot extends Entity
{
	public var sprite:Spritemap;
	private var health:Int = 5;
	private var typeNum:Int;

	public function new(x:Float, y:Float, _type:Int)
	{
		super(x, y);

		typeNum = _type;

		sprite = new Spritemap("gfx/units/basicshot.png", 1, 1);
		sprite.setFrame(0, _type);
		type = "shot" + Std.string(_type);
		graphic = sprite;
		layer = 4;
	}

	override public function update() {
		x += (typeNum == 0) ? 1 : -1;
	}
}