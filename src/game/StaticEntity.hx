package game;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;

class StaticEntity extends Entity
{
	public var sprite:Spritemap;
	private var spawn:Int = 0;

	public function new(x:Float, y:Float, _w:Int, _h:Int, _path:String, ?_layer:Int = 20)
	{
		super(x, y);

		sprite = new Spritemap("gfx/" + _path + ".png", _w, _h);
		graphic = sprite;
		layer = _layer;
	}
}