package game;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.Sfx;
import flash.geom.Point;

class Unit extends Entity
{
	private var sprite:Spritemap;
	private var typeNum:Int;
	private var shotTimer:Int = 60;
	private var health:Int = 3;

	public function new(x:Float, y:Float, _type:Int)
	{
		super(x, y);

		sprite = new Spritemap("gfx/units/basicunit.png", 12, 16);
		sprite.setFrame(0, _type);
		sprite.flipped = (_type == 0) ? false : true;
		graphic = sprite;

		typeNum = _type;

		type = "unit" + Std.string(_type);
		if (_type == 0) {
			setHitbox(6, 16, 1, 0);
		} else {
			setHitbox(6, 16, 5, 0);
		}
		layer = 5;
	}

	override public function added() {
		
	}

	override public function update()
	{
		var likeCol = collide(type, x, y);

		if (likeCol != null) {
			if (likeCol.x > x && (x - 1) > 4) {
				x -= 1;
			} else if (likeCol.x <= x && (x + 1) < 146) {
				x += 1;
			}
		}

		var nearestEntity:Entity = null;

		for (i in 1...150) {
			var unit = collide("unit" + Std.string( 1 - typeNum ), x + (i * ((typeNum == 0) ? 1 : -1)), y);

			if (unit != null) {
				nearestEntity = unit;
				break;
			}
		}

		var bases:Array<Entity> = new Array();
		scene.getType("base" + Std.string( 1 - typeNum ), bases);

		if (nearestEntity == null) {
			for (i in 0...bases.length) {
				if (y > bases[i].y && y < bases[i].y + 22) {
					nearestEntity = bases[i];
					break;
				}
			}
		} else {
			for (i in 0...bases.length) {
				if (y > bases[i].y && y < bases[i].y + 22 && Math.abs(bases[i].x - x) < Math.abs(nearestEntity.x - x)) {
					nearestEntity = bases[i];
					break;
				}
			}
		}

		if (nearestEntity != null) {
			if (Math.abs(nearestEntity.x - x) > 20) {
				if (collide(type, x + (1 * ((typeNum == 0) ? 1 : -1)), y) == null) {
					if (nearestEntity.x > x) {
						x += 0.1;
					} else {
						x -= 0.1;
					}
				} else {
					x = Math.round(x);

					if (Math.abs(nearestEntity.x - x) < 60) {
						if (shotTimer <= 0) {
							if (nearestEntity.y < y) {
								if (cast(nearestEntity, game.Base).health > 0) {
									scene.add(new Shot(x + (12 * (1 - typeNum)), y + 7, typeNum));
								}
							} else {
								scene.add(new Shot(x + (12 * (1 - typeNum)), y + 7, typeNum));
							}
							shotTimer = 60;
						}
					}
				}
			} else {
				if (shotTimer <= 0) {
					if (nearestEntity.y < y) {
						if (cast(nearestEntity, game.Base).health > 0) {
							scene.add(new Shot(x + (12 * (1 - typeNum)), y + 7, typeNum));
						}
					} else {
						scene.add(new Shot(x + (12 * (1 - typeNum)), y + 7, typeNum));
					}
					shotTimer = 60;
				}
			}
		}

		if (shotTimer > 0) {
			shotTimer -= 1;
		}

		var enemyShot = collide("shot" + Std.string( 1 - typeNum ), x, y);

		if (enemyShot != null) {
			health -= 1;
			scene.remove(enemyShot);
		}

		if (health <= 0) {
			scene.remove(this);
		}

		super.update();
	}
}