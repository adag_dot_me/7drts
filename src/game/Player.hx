package game;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.utils.Joystick;
import com.haxepunk.utils.Input;

class Player extends Entity
{
	public var sprite:Spritemap;
	private var typeNum:Int;
	private var up:Int;
	private var down:Int;
	private var select:Int;
	private var focus:Int;
	private var gamepad:Joystick;
	private var units:Array<Entity>;
	private var selectEntity:Selection;

	public function new(x:Float, y:Float, _type:Int, _up:Int, _down:Int, _select:Int, ?_gamepad:Joystick)
	{
		super(x, y);

		focus = 0;

		up = _up;
		down = _down;
		select = _select;

		units = new Array();

		sprite = new Spritemap("gfx/selection.png", 150, 56);
		sprite.setFrame(0, _type);
		type = "select" + Std.string(_type);
		setHitbox(150, 56);
		typeNum = _type;
		graphic = sprite;
		layer = 5;
	}

	override public function update()
	{
		y = focus * 56;

		if (Input.pressed(down)) {
			if (focus < 4) {
				focus += 1;
			}
		}
		if (Input.pressed(up)) {
			if (focus > 0) {
				focus -= 1;
			}
		}
		if (Input.pressed(select)) {
			trace("select pressed");
			if (!Std.is(units[0], Entity)) {
				trace("units 0 = null");
				selectEntity = new Selection(x, y, typeNum);
				scene.add(selectEntity);

				var unitArr:Array<Entity> = new Array();
				trace("array initialized");
				scene.getType("unit" + Std.string(typeNum), unitArr);
				trace("entities added to array");
				var n = 0;

				for (i in 0...unitArr.length) {
					if (unitArr[i].x > x && unitArr[i].x < x + 150 && unitArr[i].y > y && unitArr[i].y < y + 56) {
						trace("collided");
						units[n] = unitArr[i];
						n++;
					}
				}

				if (n == 0) {
					scene.remove(selectEntity);
				}
			} else {
				scene.remove(selectEntity);
				for (i in 0...units.length) {
					units[i].x = (150 * typeNum) + (( (typeNum == 0) ? 1 : -1 ) * 24);
					units[i].y = y + 36;
				}
				units = new Array();
			}
		}

		if (collide("select" + Std.string( 1 - typeNum ), x, y) != null || collide("player" + Std.string( 1 - typeNum ), x, y) != null) {
			sprite.setFrame(1, typeNum);
		} else {
			sprite.setFrame(0, typeNum);
		}

		super.update();
	}
}