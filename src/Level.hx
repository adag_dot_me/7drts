import com.haxepunk.Entity;
import com.haxepunk.Scene;
import com.haxepunk.HXP;
import com.haxepunk.graphics.Image;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import game.StaticEntity;
import game.Unit;
import game.Player;
import game.Cloud;
import game.Base;
import game.part.CloudEmitter;

class Level extends Scene
{
	var level:String;
	var units:Array<Array<Unit>>;
	var bases:Array<Array<Base>>;
	private var cloudTimer:Int;
	private var cloudEmitter:CloudEmitter;

	public function new(_level:String, ?_units:Array<Int>)
	{
		level = _level;
		cloudTimer = Std.random(300);
		cloudEmitter = new CloudEmitter();

		//Array initialization...
		units = new Array();
		for (i in 0...5) {
			units[i] = new Array();
		}

		//First timeline...
		for (i in 0...3) {
			units[0][i] = new Unit(24, 36, 0);
		}
		units[0][3] = new Unit(114, 36, 1);

		//Second and fourth timelines...
		for (i in 0...2) {
			units[1][i] = new Unit(24, 92, 0);
			units[3][i] = new Unit(24, 204, 0);
		}
		for (i in 0...2) {
			units[1][i+2] = new Unit(114, 92, 1);
			units[3][i+2] = new Unit(114, 204, 1);
		}

		//Third timeline...
		units[2][0] = new Unit(24, 148, 0);
		units[2][1] = new Unit(114, 148, 1);

		//Fifth timeline...
		units[4][0] = new Unit(24, 260, 0);
		for (i in 0...3) {
			units[4][i+1] = new Unit(114, 260, 1);
		}

		bases = new Array();
		for (i in 0...5) {
			bases[i] = new Array();
			bases[i][0] = new Base(4, 30 + (56 * i), 0);
			bases[i][1] = new Base(130, 30 + (56 * i), 1);
		}

		super();
	}

	override public function begin()
	{
		for (i in 0...5) {
			var bg = new StaticEntity(0, i * 56, 150, 56, "tiles/" + level, 20);
			add(bg);
		}

		for (i in 0...units.length) {
			for (j in 0...units[i].length) {
				add(units[i][j]);
			}
		}

		for (i in 0...bases.length) {
			for (j in 0...bases[i].length) {
				add(bases[i][j]);
			}
		}

		var overlay = new StaticEntity(0, 0, 150, 280, "overlay", 10);
		add(overlay);
		
		add(cloudEmitter);

		add(new Player(0, 0, 0, Key.W, Key.S, Key.G));
		add(new Player(0, 0, 1, Key.UP, Key.DOWN, Key.CONTROL));
		//add(new Selection(0, 0, 0));
		//add(new Selection(0, 0, 1));
	}

	override public function update()
	{
		if (Input.pressed(Key.ESCAPE)) {
			HXP.scene = new TitleScreen(4);
		}

		if (cloudTimer <= 0) {
			add(new Cloud(155, Std.random(280), cloudEmitter));
			cloudTimer = Std.random(300);
		} else {
			cloudTimer -= 1;
		}

		super.update();
	}
}